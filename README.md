# README #

Steps necessary to get application up and running.

### What is this repository for? ###

* Filtering matcher Spark project
* Version 1.0


### How do I get set up? ###

* git clone git@bitbucket.org:viancu/filteringmatches.git
* cd filteringmatches
* ./mvnw package
* ./mvnw integration-test
* java -jar target/filtering-matches-1.0.jar
* open http://localhost:8080

### Development ###

* Focused on backend mostly
* Used json schema
