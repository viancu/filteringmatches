var path = require('path');

module.exports = {
    entry: './src/main/js/index.js',
    cache: true,
    output: {
        path: __dirname,
        filename: './src/main/resources/static/built/app.js'
    },
    module: {
        rules: [
            {
                test: path.join(__dirname, '.'),
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            }
        ]
    }
};