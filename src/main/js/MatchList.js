import React from "react";
import PropTypes from "prop-types";

const MatchList = (props) => {
    const matches = props.matches;
    const matchList = matches.map(match => {
        var favStyle = {
            color: 'red',
        };
        return (

            <li className="list-group-item" key={match.id}>
                <div className="media">
                    <div className="media-left">
                        <img className="media-object" src={match.main_photo} width={100} height={100}/>
                    </div>
                    <div className="media-body">
                        <h4 className="media-heading">{match.display_name} {match.favourite && <span className="glyphicon glyphicon-heart" style={favStyle}></span>}</h4>
                        <div className="row">
                            <div className="col-sm-4">{match.age} years</div>
                            <div className="col-sm-4">{match.job_title}</div>
                            <div className="col-sm-2">{match.height_in_cm} cm</div>
                            <div className="col-sm-2">{match.religion}</div>
                        </div>
                        <div className="row">
                            <div className="col-sm-4">from {match.city.name}</div>
                            <div className="col-sm-4">{match.compatibility_score * 100}% compatibility </div>
                            <div className="col-sm-4">{match.contacts_exchanged > 0 && <span>{match.contacts_exchanged} friends in common</span>}</div>

                        </div>
                    </div>
                </div>
            </li>
        );
    });

    return (
        <ul className="list-group">{matchList}</ul>
    );
};

MatchList.propTypes = {
    matches: PropTypes.array
};

export default MatchList;