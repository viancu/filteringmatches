import React from "react";
import Modal from "react-modal";
import Filter from "./Filter";
import MatchList from "./MatchList";
import FilterButton from "./FilterButton";

const FilterService = require('./filter-service');

class MatchManager extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            matches: [],
            isFilterModalOpen: false        };

        this.handleOnCloseFilterModal = this.handleOnCloseFilterModal.bind(this);
        this.handleOpenFilterModal = this.handleOpenFilterModal.bind(this);
        this.handleOnSubmitFilter = this.handleOnSubmitFilter.bind(this);
        }

    componentDidMount() {
        this.listMatches();
    }

    componentWillMount() {
        Modal.setAppElement('body');
    }

    listMatches() {
        FilterService
            .listMatches({})
            .then(matches => {
                this.setState({matches});
                return;
            })
            .catch(error => {
                console.log(error);
                return;
            });
    }

    handleOnCloseFilterModal() {
        this.setState({isFilterModalOpen: false});
    }

    handleOpenFilterModal() {
        this.setState({isFilterModalOpen: true});
    }

    handleOnSubmitFilter(formData) {
    console.log(formData);
        FilterService
            .listMatches(formData)
            .then(matches => {
                formData.done = true;
                this.setState({formData});
                this.setState({matches});
                this.setState({isFilterModalOpen: false});
            })
            .catch(error => console.log(error));

    }
    render() {
        return (
            <div className="row">
                <div className="col-sm-6">
                <Modal isOpen={this.state.isFilterModalOpen} onRequestClose={this.handleOnCloseFilterModal}>
                    <Filter onSubmitFilter={this.handleOnSubmitFilter}
                            onCloseModal={this.handleOnCloseFilterModal}
                    />
                </Modal>
                <div className="col-sm-12">
                    <FilterButton openFilterModal={this.handleOpenFilterModal}/>
                </div>
                <div className="col-sm-12">
                    <MatchList matches={this.state.matches}/>
                </div>
                </div>
            </div>
        );
    }
}

export default MatchManager;