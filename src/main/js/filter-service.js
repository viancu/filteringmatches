'use strict';
import * as axios from 'axios';


const listMatches = (filter) => {

    return new Promise((resolve, reject) => {
        axios
            .post('/filter', filter)
            .then((result) => {
                resolve(result.data);
            })
            .catch(error => {
                console.log(error);
                reject(error.message);
            });

    });

};

const filterSchema = () => {
    return new Promise((resolve, reject) => {
        axios
            .get('/schema')
            .then((result) => {
                resolve(result.data);
            })
            .catch(error => {
                console.log(error);
                reject(error.message);
            });

    });
}

module.exports = {
    'listMatches': listMatches,
    'filterSchema': filterSchema
};