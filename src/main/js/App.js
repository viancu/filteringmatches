import React from "react";
import MatchManager from "./MatchManager";

class App extends React.Component {
    constructor() {
        super();
        this.state = {}
    }

    render() {
        return (
            <MatchManager />
        );
    }
}

export default App;
