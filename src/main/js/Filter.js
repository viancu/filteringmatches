import React from "react";
import Form from "react-jsonschema-form";
import PropTypes from "prop-types";

const FilterService = require('./filter-service');

class Filter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            schema: {},
            formData: {}
        };
    }

    componentDidMount() {
        this.loadSchema();
    }

    loadSchema() {
        FilterService
            .filterSchema()
            .then(schema => {
                this.setState({schema});
                return;
            })
            .catch(error => {
                console.log(error);
                return;
            });
    }


    render() {

        const uiSchema = {
            has_photo: {
                "ui:widget": "radio",
                classNames: "form-inline"
            },
            in_contact: {
                "ui:widget": "radio"
            },
            favourite: {
                "ui:widget": "radio"
            },
            compatibility_score_min: {
                "ui:widget": "range"
            },
            compatibility_score_max: {
                "ui:widget": "range"
            },
            age_min: {
                "ui:widget": "range"
            },
            age_max: {
                "ui:widget": "range"
            },
            height_min: {
                "ui:widget": "range"
            },
            height_max: {
                "ui:widget": "range"
            }
        }
        const log = (type) => console.log.bind(console, type);

        const onSubmit = ({formData}) => this.props.onSubmitFilter(formData);

            return (
                    <Form schema={this.state.schema}
                     uiSchema={uiSchema}
                     onSubmit={onSubmit}>
                    </Form>
            );
    }
}

Filter.propTypes = {
    onCloseModal: PropTypes.func,
    onSubmitFilter: PropTypes.func,
    formData: PropTypes.object
};

export default Filter;