import React from "react";
import PropTypes from "prop-types";

class FilterButton extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
<p>
            <button className="btn btn-primary btn-lg btn-block" type="button" onClick={this.props.openFilterModal}>
                Filter
            </button>
</p>
        );
    }
}

FilterButton.propTypes = {
    openFilterModal: PropTypes.func
};

export default FilterButton;