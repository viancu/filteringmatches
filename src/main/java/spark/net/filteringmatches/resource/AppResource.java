package spark.net.filteringmatches.resource;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import spark.net.filteringmatches.filter.FilterService;
import spark.net.filteringmatches.filter.MatchFilter;
import spark.net.filteringmatches.match.Match;
import spark.net.filteringmatches.match.MatchService;

import java.io.IOException;

@Controller
public class AppResource {

    @Autowired
    MatchService matchService;

    @Autowired
    FilterService filterService;

    @GetMapping("/")
    public String app() {
        return "index";
    }

    @CrossOrigin(origins = "*")
    @PostMapping("/filter")
    public ResponseEntity<?> filter(@RequestBody MatchFilter filter) {


        if (filterService.validateFilterMatchSchema(filter)) {
            Match match = (filter.getDistance() == null) ? null : matchService.getLoggedInUser();
            return new ResponseEntity<>(matchService.getMatches(filter, match), HttpStatus.OK);
        }

        return new ResponseEntity<>("bad json", HttpStatus.BAD_REQUEST);


    }

    @CrossOrigin(origins = "*")
    @GetMapping(value = "/schema")
    @ResponseBody
    public JsonNode loadSchema() throws IOException {
        return filterService.loadFilterMatchesSchema();
    }


}
