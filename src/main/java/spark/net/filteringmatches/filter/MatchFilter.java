package spark.net.filteringmatches.filter;

import lombok.Data;

import java.util.UUID;

@Data
public class MatchFilter {
    private Boolean hasPhoto;
    private Boolean inContact;
    private Boolean favourite;
    private Integer compatibilityScoreMin;
    private Integer compatibilityScoreMax;
    private Integer ageMin;
    private Integer ageMax;
    private Integer heightMin;
    private Integer heightMax;
    private Integer distance;
    private UUID excludedId;
}
