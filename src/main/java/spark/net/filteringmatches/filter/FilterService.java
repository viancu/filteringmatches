package spark.net.filteringmatches.filter;
import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;
import com.fasterxml.jackson.databind.JsonNode;

import com.github.fge.jsonschema.util.JsonLoader;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;

@Service
public class FilterService {

    @Value("${app.schema.uri}")
    private String schemaResource;

    public Boolean validateFilterMatchSchema(MatchFilter filter) {
        try (InputStream inputStream = getClass().getResourceAsStream(schemaResource)) {
            JSONObject rawSchema = new JSONObject(new JSONTokener(inputStream));
            Schema schema = SchemaLoader.load(rawSchema);
            schema.validate(new JSONObject(filter));
            return true;
        } catch (IOException | ValidationException e) {
            return false;
        }
    }

    public JsonNode loadFilterMatchesSchema() throws IOException {
        return JsonLoader.fromResource(schemaResource);
    }
}
