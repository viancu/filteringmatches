package spark.net.filteringmatches.match;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spark.net.filteringmatches.filter.MatchFilter;
import spark.net.filteringmatches.specification.Haversine;
import spark.net.filteringmatches.specification.MatchSpecification;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MatchService {

    @Autowired
    MatchSpecification matchSpecification;

    @Autowired
    private MatchRepository matchRepository;


    public Iterable<Match> save(List<Match> matches) {
        return matchRepository.save(matches);
    }

    public List<Match> getMatches(MatchFilter filter) {
        List<Match> results = matchRepository.findAll(matchSpecification.getFilter(filter));

        return results;
    }

    public List<Match> getMatches(MatchFilter filter, Match signedInMatch) {
        if (signedInMatch != null) {
            filter.setExcludedId(signedInMatch.getId());
        }
        List<Match> results = getMatches(filter);

        if (filter.getDistance() != null && signedInMatch != null) {
            results = filterByDistance(filter.getDistance(), results, signedInMatch);
        }
        return results;
    }

    public Match getLoggedInUser() {
       return matchRepository.findOneByDisplayName("Katie");
    }

    private List<Match> filterByDistance(Integer distance, List<Match> results, Match signedInMatch) {
        return results.stream().filter(m ->
                (distance <= 30 && Haversine.distance(signedInMatch.getCity().getLat(), signedInMatch.getCity().getLon(), m.getCity().getLat(), m.getCity().getLon()) <= 30)
                        || (distance > 30 && distance < 300 && Haversine.distance(signedInMatch.getCity().getLat(), signedInMatch.getCity().getLon(), m.getCity().getLat(), m.getCity().getLon()) <=  distance)
                        || (distance >= 300 ))
                .collect(Collectors.toList());
    }
}
