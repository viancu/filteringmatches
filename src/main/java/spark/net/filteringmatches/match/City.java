package spark.net.filteringmatches.match;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Embeddable;

@Embeddable
public class City {

    @Getter @Setter
    private String name;

    @Getter @Setter
    private Double lat;

    @Getter @Setter
    private Double lon;
}
