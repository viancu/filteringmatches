package spark.net.filteringmatches.match;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@AllArgsConstructor
@EqualsAndHashCode(exclude = {"id", "city"})
@Entity
public class Match {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id", updatable = false, nullable = false)
    @Getter
    private UUID id;

    @Getter
    @Setter
    private String displayName;

    @Getter
    @Setter
    private Integer age;

    @Getter
    @Setter
    private String jobTitle;

    @Getter
    @Setter
    private Integer heightInCm;

    @Getter
    @Setter
    @Embedded
    private City city;

    @Getter
    @Setter
    private String mainPhoto;

    @Getter
    @Setter
    private Float compatibilityScore;

    @Getter
    @Setter
    private Integer contactsExchanged;

    @Getter
    @Setter
    private boolean favourite;

    @Getter
    @Setter
    private String religion;

    public Match() {}
}

