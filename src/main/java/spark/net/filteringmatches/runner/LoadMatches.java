package spark.net.filteringmatches.runner;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import spark.net.filteringmatches.match.Match;
import spark.net.filteringmatches.match.MatchService;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@Component
public class LoadMatches implements CommandLineRunner {

    @Autowired
    private MatchService matchService;

    @Value("${app.matches.uri}")
    private String matchesResource;


    @Override
    public void run(String... strings) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        TypeReference<List<Match>> typeReference = new TypeReference<List<Match>>() {
        };
        InputStream inputStream = TypeReference.class.getResourceAsStream(matchesResource);
        try {
            JsonNode json = mapper.readTree(inputStream).get("matches");
            List<Match> matches = mapper.readValue(json.toString(), typeReference);
            matchService.save(matches);
            System.out.println("Matches loaded.");
        } catch (IOException e) {
            System.out.println("Error loading matches.");
        }
    }
}
