package spark.net.filteringmatches.specification;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import spark.net.filteringmatches.filter.MatchFilter;
import spark.net.filteringmatches.match.Match;

import java.util.UUID;

import static org.springframework.data.jpa.domain.Specifications.where;

@Component
public class MatchSpecification {

    public Specification<Match> getFilter(MatchFilter filter) {
        return (root, query, cb) -> where(oneEqualsOne())
                .and(hasPhoto(filter.getHasPhoto()))
                .and(favourite(filter.getFavourite()))
                .and(inContact(filter.getInContact()))
                .and(ageMin(filter.getAgeMin()))
                .and(ageMax(filter.getAgeMax()))
                .and(heightMin(filter.getHeightMin()))
                .and(heightMax(filter.getHeightMax()))
                .and(compatibilityScore("compatibilityScore", filter.getCompatibilityScoreMin(), "min"))
                .and(compatibilityScore("compatibilityScore", filter.getCompatibilityScoreMax(), "max"))
                .and(excludedId(filter.getExcludedId()))
                .toPredicate(root, query, cb);
    }

    private Specification<Match> oneEqualsOne() {
        return (root, query, cb) -> cb.equal(cb.literal(1), 1);
    }


    private Specification<Match> hasPhoto(Boolean hasPhoto) {
        return (root, query, cb) -> {
            if (hasPhoto == null) {
                return null;
            }
            if (hasPhoto) {
                return cb.isNotNull(root.get("mainPhoto"));
            } else {
                return cb.isNull(root.get("mainPhoto"));
            }
        };
    }

    private Specification<Match> favourite(Boolean favourite) {
        return (root, query, cb) -> {
            if (favourite == null) {
                return null;
            }
            if (favourite) {
                return cb.isTrue(root.get("favourite"));
            } else {
                return cb.isFalse(root.get("favourite"));
            }
        };
    }

    private Specification<Match> inContact(Boolean inContact) {
        return (root, query, cb) -> {
            if (inContact == null) {
                return null;
            }
            if (inContact) {
                return cb.gt(root.get("contactsExchanged"), 0);
            } else {
                return cb.equal(root.get("contactsExchanged"), 0);
            }
        };
    }

    private Specification<Match> ageMin(Integer age) {
        return rangeCriteriaMin("age", age);
    }

    private Specification<Match> ageMax(Integer age) {
        return rangeCriteriaMax("age", age);
    }

    private Specification<Match> heightMin(Integer height) {
        return rangeCriteriaMin("heightInCm", height);
    }

    private Specification<Match> heightMax(Integer height) {
        return rangeCriteriaMax("heightInCm", height);
    }

    private Specification<Match> compatibilityScore(String field, Integer value, String limit) {
        return (root, query, cb) -> {
            if (value == null || value == 0) {
                return null;
            }
            Float score = (float) value / 100;

            if (limit.equals("min")) {
                return cb.greaterThanOrEqualTo(root.get(field), score);
            } else if (limit.equals("max")) {
                return cb.lessThanOrEqualTo(root.get(field), score);
            }
            return null;
        };
    }

    private Specification<Match> rangeCriteriaMin(String field, Integer value) {
        return (root, query, cb) -> {
            if (value == null) {
                return null;
            }

            return cb.greaterThanOrEqualTo(root.get(field), value);
        };
    }

    private Specification<Match> rangeCriteriaMax(String field, Integer value) {
        return (root, query, cb) -> {
            if (value == null) {
                return null;
            }

            return cb.lessThanOrEqualTo(root.get(field), value);
        };
    }

    private Specification<Match> excludedId(UUID id) {
        return (root, query, cb) -> {
            if (id == null) {
                return null;
            }
            return cb.notEqual(root.get("id"), id);
        };
    }
}
