package spark.net.filteringmatches;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import spark.net.filteringmatches.filter.MatchFilter;
import spark.net.filteringmatches.match.Match;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;

public class FilterStepdefs extends AppSpringIntegration{
    MatchFilter filter;
    List<Match> matches;
    Match excludedMatch;

    @Given("^hasPhoto is set")
    public void filterHasPhoto() throws Throwable {
        filter = new MatchFilter();
        filter.setHasPhoto(true);
    }

    @Given("^heightMin is (\\d+) cm$")
    public void heightMin(Integer arg0) throws Throwable {
        filter = new MatchFilter();
        filter.setHeightMin(arg0);
    }

    @Given("^heightMax is (\\d+) cm$")
    public void heightMax(Integer arg0) throws Throwable {
        filter = new MatchFilter();
        filter.setHeightMax(arg0);
    }

    @Given("^inContact is set$")
    public void filterInContactIsTrue() throws Throwable {
        filter = new MatchFilter();
        filter.setInContact(true);
    }

    @Given("^favourite is set$")
    public void filterFavourite() throws Throwable {
        filter = new MatchFilter();
        filter.setFavourite(true);
    }

    @Given("^ageMin is (\\d+)$")
    public void ageMin(int arg0) throws Throwable {
        filter = new MatchFilter();
        filter.setAgeMin(arg0);
    }

    @Given("^ageMax is (\\d+)$")
    public void ageMax(int arg0) throws Throwable {
        filter = new MatchFilter();
        filter.setAgeMax(arg0);
    }

    @Given("^compatibilityScoreMin is (\\d+)$")
    public void compatibilityScoreMin(int arg0) throws Throwable {
        filter = new MatchFilter();
        filter.setCompatibilityScoreMin(arg0);
    }

    @Given("^compatibilityScoreMax is (\\d+)$")
    public void compatibilityScoreMax(int arg0) throws Throwable {
        filter = new MatchFilter();
        filter.setCompatibilityScoreMax(arg0);
    }

    @When("^filter is applied$")
    public void filterIsRun() throws Throwable {
        matches = matchService.getMatches(filter);
    }

    @Then("^return (\\d+) matches$")
    public void returnMatches(Integer arg0) throws Throwable {
        assertThat(matches.size(), equalTo(arg0));
    }

    @Given("^excludedId is set$")
    public void excludedidIsSet() throws Throwable {
        excludedMatch  = matchRepository.findFirstByOrderByIdAsc();
        filter = new MatchFilter();
        filter.setExcludedId(excludedMatch.getId());
    }

    @Then("^results do not contain excludedId$")
    public void resultsDoNotContainExcludedId() throws Throwable {
        assertThat(matches, not(hasItem(excludedMatch)));
    }

    @Given("^distance is (\\d+) km$")
    public void distanceIsKm(int arg0) throws Throwable {
        filter = new MatchFilter();
        filter.setDistance(arg0);
    }

    @When("^distance filter is applied$")
    public void filterWithDistanceIsApplied() throws Throwable {
        Match signedInMatch  = matchRepository.findOneByDisplayName("Kate");
        matches = matchService.getMatches(filter, signedInMatch);
    }
}
