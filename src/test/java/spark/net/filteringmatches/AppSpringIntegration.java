package spark.net.filteringmatches;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootContextLoader;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import spark.net.filteringmatches.match.MatchRepository;
import spark.net.filteringmatches.match.MatchService;

@ActiveProfiles( profiles={"test"})
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = App.class, loader = SpringBootContextLoader.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AppSpringIntegration {
    @Autowired
    MatchRepository matchRepository;

    @Autowired
    MatchService matchService;
}
