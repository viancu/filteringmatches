package spark.net.filteringmatches;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class LoadMatchesStepdefs extends AppSpringIntegration {

    @Then("^the matches data is loaded$")
    public void theMatchesDataIsLoaded() throws Throwable {
        assertThat(matchRepository.count(), greaterThan(0L));
    }

    @Given("^the application is started$")
    public void theApplicationIsStarted() throws Throwable {
       //do nothing
    }
}
