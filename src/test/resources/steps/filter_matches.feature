Feature: Filter matches

  Scenario: Has photo
    Given hasPhoto is set
    When filter is applied
    Then return 22 matches

  Scenario: In contact
    Given inContact is set
    When filter is applied
    Then return 12 matches

  Scenario: Favourite
    Given favourite is set
    When filter is applied
    Then return 6 matches

  Scenario: Height min
    Given heightMin is 177 cm
    When filter is applied
    Then return 3 matches

  Scenario: Height max
    Given heightMax is 153 cm
    When filter is applied
    Then return 13 matches

  Scenario: Age min
    Given ageMin is 40
    When filter is applied
    Then return 14 matches

  Scenario: Age max
    Given ageMax is 30
    When filter is applied
    Then return 1 matches

  Scenario: Compatibility score min
    Given compatibilityScoreMin is 88
    When filter is applied
    Then return 11 matches

  Scenario: Compatibility score max
    Given compatibilityScoreMax is 73
    When filter is applied
    Then return 2 matches

  Scenario: Exclude match from results
    Given excludedId is set
    When filter is applied
    Then results do not contain excludedId

  Scenario: Distance < 30 km
    Given distance is 29 km
    When distance filter is applied
    Then return 0 matches

  Scenario: Distance between 30 km and 300 km
    Given distance is 200 km
    When distance filter is applied
    Then return 17 matches

  Scenario: Distance > 300 km
    Given distance is 500 km
    When  distance filter is applied
    Then return 24 matches




